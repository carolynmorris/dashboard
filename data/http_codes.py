from influxdb.influxdb08 import InfluxDBClient
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

host = '54.165.160.150'
port = 8086
user = 'modulus'
password = 'fQHVDX7xbEfc8U'
database = 'mod_stats'
client = InfluxDBClient(host, port, user, password, database)
servo = '"servo.e2e5e891-3ec2-4c5c-bc22-6526459e0d6d"'


# two times that are 24 hrs apart
first_time = "1433097795"
last_time  = "1433184148"

# # get the distinct codes for the last hour
# qd = 'select distinct(code) as code from '+servo+' where time > '+first_time+'s and time < '+last_time+'s;'

# rd = client.query(qd, time_precision="ms")

# dta = rd[0]
# df = pd.DataFrame(dta['points'], columns=dta['columns'])
# codes = df['code']

# df2 = pd.DataFrame()

# for i in codes:
# 	q = 'select time, count(code) as value from '+servo+' \
# 		 group by time(60m) fill(0) where code = '+str(i)+' \
# 		 and time > '+first_time+'s and time < '+last_time+'s;'
# 	r = client.query(q, time_precision="ms")
# 	data = r[0]
# 	for j in data['points']:
# 		j.append(i)
# 	df3 = pd.DataFrame(data['points'], columns=data['columns']+['name'])
# 	df2 = df2.append(df3)
# #df2.to_json('httpCodes.json', orient='records')

# # response times
# qt = 'select time, mean(value) as value from '+servo+' \
# 	  group by time(60m) fill(0) \
# 	  where time > '+first_time+'s and time < '+last_time+'s;'
# rt = client.query(qt, time_precision="ms")
# dt = rt[0]

# df4 = pd.DataFrame(dt['points'], columns=dt['columns'])
# #df4.to_json('responseTimes.json', orient='records')

# # print df2
# # print '\n'
# # print df4

# q = 'select time, count(route) from '+servo+' \
# 	 group by time(60m) fill(0) \
# 	 where time > '+first_time+'s and time < '+last_time+'s;'
# r = client.query(q, time_precision="ms")
# d = pd.DataFrame(r[0]['points'], columns=r[0]['columns'])


# plt.figure();
# d.plot(x='time', y='count')
# print d
# #df2.plot(x='time', y='value'); 
# df4.plot(x='time', y='value'); 
# plt.show();

# qx = 'select * from '+servo+' \
# where time > '+first_time+'s and time < '+last_time+'s;'

# servop = '"servo.process.e2e5e891-3ec2-4c5c-bc22-6526459e0d6d"'

# q = 'select mean(value) as value, sum(bandwidth) as bandwidth from '+servo+' \
# 	group by time(60m) fill(0) \
# 	where time > '+first_time+'s and time < '+last_time+'s;'

# # # 10 slowest routes
# # q = 'select time, distinct(value) as value from '+servo+' \
# # 	where time > '+first_time+'s and time < '+last_time+'s;'
# # #q = 'select percentile(value, 50), mean(value) from '+servo+';'

# r = client.query(q)
# df = pd.DataFrame(r[0]['points'], columns=r[0]['columns'])
# # df = df.sort('value', ascending=False)

# df['time'] = pd.to_datetime(df['time'], unit='s')
# print df['time']

# plt.figure();
# df.plot(x='time', y='value', label='time');
# df.plot(x='time', y='bandwidth', label='bandwidth');
# plt.show();

# plot bandwidth and 

# vals = df[0:10]['value']
# vals = np.array(vals)


# dframe = pd.DataFrame()
# for v in vals:
# 	print v 
# 	q2 = 'select route, value, bandwidth from '+servo+' where value = '+str(v)+' and \
# 	time > '+first_time+'s and time < '+last_time+'s;'
# 	r2 = client.query(q2, time_precision="ms")
# 	df = pd.DataFrame(r2[0]['points'], columns=r2[0]['columns'])
# 	dframe = dframe.append(df)

# print dframe
# print '\n'
#dframe.to_json('slowest.json', orient='records')
# print df.describe()

# print '\n'

# first_time = "1433030400"
# last_time = "1433116800"

servo_reg = 'servo.e2e5e891-3ec2-4c5c-bc22-6526459e0d6d'
servo_process = 'servo.process.e2e5e891-3ec2-4c5c-bc22-6526459e0d6d'

# CPU
def query(columns, servo, time1, time2, fileName):
	q = 'select '+columns+' \
			from "'+servo+'" \
			group by time(60m) \
			where time > '+time1+'s and time < '+time2+'s;'
	r = client.query(q, time_precision='ms')
	df = pd.DataFrame(r[0]['points'], columns=r[0]['columns'])
	df.to_json(fileName, orient='records')

query('mean(cpu_user) as value', servo_process, first_time, last_time, 'cpu_user.json')
query('mean(cpu_system) as value', servo_process, first_time, last_time, 'cpu_system.json')

query('mean(memory_rss) as value', servo_process, first_time, last_time, 'memory_rss.json')
query('mean(memory_swap) as value', servo_process, first_time, last_time, 'memory_swap.json')

query('mean(bandwidth) as value', servo_reg, first_time, last_time, 'bandwidth.json')
