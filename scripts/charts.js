function addName(series, seriesName) {
  for (var i=0; i<series.length; i++) {
    series[i].name = seriesName;
  }
  return series;
}

function combineSeries(series1, series2) {
  var combinedSeries  = _.zip(series1, series2);
  var flattenedSeries = _.flatten(combinedSeries);
  return flattenedSeries;
}

function createChart(chartID, data, chartType, stacked, title, legend, toolTip, scaleUp) {
  $(chartID).kendoChart({
    dataSource: {
      data: data,
      group: {
        field: "name"
      },
      sort: {
        field: "time",
        dir: "asc"
      } 
    },
    seriesDefaults: {
      type: chartType,
      stack: stacked
    },
    series: [{
      field: "value",
      categoryField: "time"
    }],
    legend: {
      visible: legend,
      position: "bottom",
    },
    title: {
      text: title,
      align: "left",
      font: "14px Karla"
    },
    categoryAxis: {
      type: "date",
      baseUnit: "minutes",  // this should change depending on 
      baseUnitStep: 60,     // selected date range 
      labels: {
        format: "h:mm tt",
        rotation: 0,
        step: 4, // for 24 hour data.
        font: "10px Helvetica",
        color: "lightgrey" 
      },
      majorGridLines: {
        visible: false
      },
      line: {
        color: "lightgrey"
      }
    },
    valueAxis: {
      labels: {
        step: 2,
        color: "lightgrey"
      },
      line: {
        visible: false
      },
      majorGridLines: {
        visible: false
      },
      crosshair: { 
        visible: false
      }
    },
    tooltip: {
      visible: true,
      shared: false,
      template: toolTip
    },
    theme: "metroBlack"
  });
}

var tooltipDate = "kendo.format(\'{0:MMM d, h:mm tt }\', category)";

$(document).ready(function() {
  var rss    = null
    , swap   = null
    , memory = null;

  $.getJSON('data/memory_rss.json')
    .then(function(data) {
      rss = data;
      return $.getJSON('data/memory_swap.json');
    })
    .then(function(data) {
      // All AJAX requests are complete.
      swap = data;

      rss = addName(rss, "memory_rss");
      swap = addName(swap, "memory_swap");

      memory = combineSeries(rss, swap);

      // sample data is in bytes, convert to MB
      for (i in memory) {
        memory[i]['value']*=(1/1000000);
      };

      createChart("#chartMemory", memory, "area", true, 
                  "Memory Usage (MB)", true, 
                  "#= kendo.toString(value, 'n2')# MB <br /> #="+tooltipDate+"#",
                  200);
    })
    .fail(console.error.bind(console, 'Error:'));

  var user   = null
    , system = null
    , cpu    = null;

  $.getJSON('data/cpu_user.json')
    .then(function(data) {
      user = data;
      return $.getJSON('data/cpu_system.json')
    })
    .then(function(data) {
      system = data;

      user = addName(user, "cpu_user");
      system = addName(system, "cpu_system");

      cpu = combineSeries(user, system);

      createChart("#chartCPU", cpu, "column", true, "CPU Usage (%)", true, 
                  "#= kendo.toString(value, 'n2') #% <br /> #="+tooltipDate+"# ",
                  90);
    })
    .fail(console.error.bind(console, 'Error:'));

  var responseTimes = null;
  $.getJSON('data/responseTimes.json')
    .then(function(data) {
      responseTimes = data;
      createChart("#chartResponse", responseTimes, "column", false, 
                  "Response Time (ms)", false, 
                  "#= kendo.toString(value, 'n2') # ms <br /> #="+tooltipDate+"#",
                  1000);
    });

  var responseCount = null;
  $.getJSON('data/httpCodes.json')
    .then(function(data) {
      responseCount = data;
      createChart("#chartHTTP", responseCount, "column", true, "Requests", true, 
                  "#= kendo.toString(value) # Requests <br /> #="+tooltipDate+"#");
    });

});

//
// Responsive charts
//

$(window).on("resize", function() {
      kendo.resize($(".chart-wrapper"));
});

//
// Menu
//

$(document).ready(function() {
    $("#menu").kendoMenu();

    $("#datepicker").kendoDateTimePicker({
      value: new Date()
    });
});

//
// Page Scroll to ID
//

// You can avoid the document.ready if you put the script at the bottom of the page
$(document).ready(function() {
  // Bind to the click of all links with a #hash in the href
  $('a[href^="#"]').click(function(e) {
    // Prevent the jump and the #hash from appearing on the address bar
    e.preventDefault();
    // Scroll the window, stop any previous animation, stop on user manual scroll
    // Check https://github.com/flesler/jquery.scrollTo for more customizability
    $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true});
  });
});
