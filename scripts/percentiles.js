$("#chart").kendoChart({
    theme: "metroBlack",
    title: {
        text: "Response Times (ms)",
        align: "left",
        font: "14px Helvetica"
    },
    legend: {
        position: "bottom"
    },
    seriesDefaults: {
        type: "line",
        //area: {
            //line: {
                //style: "line"
            //}
        //}
    },
    series: [{
        name: "Mean",
        data: [3.359788359788362, 2.9663915094339592, 0, 0, 2.1870869344178967, 2.1676012461059213, 3.8943313153549775, 2.606683804627242, 2.1406826568265704, 4.743796884016149, 3.3858208955223876, 3.661198738170353, 2.4465691788526396, 2.1607427055702892, 2.8412042502951587, 3.210323574730359, 2.905102954341984, 4.645047169811303, 6.123940677966112, 3.0798226164079847, 3.02007528230866, 2.650815217391307, 2.49583333333333, 2.362002567394092]
    }, {
        name: "Median",
        data: [1, 1, 0, 0, 1, 1, 2, 2, 1, 2, 1, 2, 1, 1, 1, 2, 1, 2, 2, 1, 2, 1, 2, 2]
    }, {
        name: "95th Percentile",
        data: [5, 4, 0, 0, 4, 4, 5, 5, 4, 6, 4, 5, 4, 4, 5, 4, 5, 8, 11, 5, 4, 4, 5, 4]
    }],
    valueAxis: {
        labels: {
            format: "{0}"
        },
        line: {
            visible: false
        },
        //axisCrossingValue: -10
    },
    categoryAxis: {
        //categories: [2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011],
        majorGridLines: {
            visible: false
        }
    },
    tooltip: {
        visible: true,
        format: "{0}",
        template: "#= series.name #: #= kendo.toString(value, 'n2') # ms" 
    }
});
